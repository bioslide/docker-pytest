# Docker file for testing of the slideio python package

Based image: continuumio/anaconda3
The image creates a new conda environment "slideio" with all extra packages used in the testing.
## Installed python packages:
- numpy
- opencv
- czifile
- imagecodecs
- pytest
- tifffiles
## How to use the container for runnin of slideio python tests:
- Switch to a folder you want to mount to the container.
- Start the container in daemon mode.
- - For Windows hosts:
```
docker run -dit --rm -v ${PWD}:/projects --name pytest booritas/slideio-pytests bash
```
- - For Linux hosts (the difference only in the "pwd" variable)"
```
docker run -dit --rm -v $(pwd):/projects --name pytest booritas/slideio-pytests bash
```
- Connect to the container:
```
docker exec -it pytest bash
```
- Setup location of the test images (put correct path to the location of test repositories in the snipped below):
```
    source /projects/slideio_extra/env_vars.sh
    source /projects/slideio_extra_priv/env_vars.sh
```
- Activate the slideio environment:
```
conda activate slideio
```
- Install slideio package:
```
python -m pip install {path to the whl file}
```
- Switch to the slideio/src/pytests directory.
- Run the tests
```
pytest . --verbose -s
```
- Exit the container (Ctrl+D)
- Stop the container if you don't need it anymore.
```
docker kill pytest
```